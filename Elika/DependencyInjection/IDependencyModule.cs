﻿using Microsoft.Extensions.DependencyInjection;

namespace Elika.DependencyInjection
{
    /// <summary>
    ///     Модуль с зависимостями
    /// </summary>
    public interface IDependencyModule
    {
        /// <summary>
        ///     Зарегистрировать модуль в коллекции сервисов
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        void Register(IServiceCollection services);
    }
}
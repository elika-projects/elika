﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Elika.DependencyInjection
{
    /// <summary>
    ///     Расширения для <see cref="IServiceProvider"/>
    /// </summary>
    public static class ServiceProviderExtensions
    {
        /// <summary>
        ///     Создать экземляр объекта типа <typeparamref name="T"/> ипользуя аргументы конструктора из <paramref name="serviceProvider"/> и <paramref name="parameters"/>
        /// </summary>
        /// <typeparam name="T">Тип экземпляра</typeparam>
        /// <param name="serviceProvider">Провайдер сервисов</param>
        /// <param name="parameters">Параметры</param>
        /// <returns>Экземпляр типа <typeparamref name="T"/></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static T CreateInstance<T>(this IServiceProvider serviceProvider, params object[] parameters)
        {
            if(serviceProvider == null)
                throw new ArgumentNullException(nameof(serviceProvider));

            
            var instance = ActivatorUtilities.CreateInstance<T>(serviceProvider, parameters);
            return instance;
        }

        /// <summary>
        ///     Получить сервис или создать экземляр объекта типа <typeparamref name="T"/> ипользуя аргументы конструктора из <paramref name="serviceProvider"/>
        /// </summary>
        /// <typeparam name="T">Тип экземпляра</typeparam>
        /// <param name="serviceProvider">Провайдер сервисов</param>
        /// <returns>Экземпляр типа <typeparamref name="T"/></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static T GetServiceOrCreateInstance<T>(this IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
                throw new ArgumentNullException(nameof(serviceProvider));

            var instance = ActivatorUtilities.GetServiceOrCreateInstance<T>(serviceProvider);
            return instance;
        }
    }
}

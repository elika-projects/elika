﻿using System;
using System.Linq;
using Elika.Data;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Elika.DependencyInjection
{
    /// <summary>
    ///     Расширения для <see cref="IServiceCollection" />
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        ///     Добавить модуль в коллекцию сервисов
        /// </summary>
        /// <typeparam name="TModule">Тип модуля</typeparam>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекция сервисов</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static IServiceCollection AddModule<TModule>(this IServiceCollection services)
            where TModule : class, IDependencyModule, new()
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            var module = new TModule();
            return services.AddModule(module);
        }

        /// <summary>
        ///     Добавить модуль в коллекцию сервисов
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <param name="module">Тип модуля</param>
        /// <returns>Коллекция сервисов</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static IServiceCollection AddModule(this IServiceCollection services, IDependencyModule module)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            if (module == null)
                throw new ArgumentNullException(nameof(module));

            module.Register(services);
            return services;
        }

        /// <summary>
        ///     Найти дескрипторы наследуемые от универсального типа <paramref name="parentType"/>
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <param name="parentType">Родительский тип</param>
        /// <returns>Дескрипторы</returns>
        /// <exception cref="ArgumentNullException"></exception>
        private static ServiceDescriptor[] FindByParentOf(this IServiceCollection services,
            Type parentType)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            if (parentType == null)
                throw new ArgumentNullException(nameof(parentType));

            var descriptors = services
                .Where(d => d.ServiceType.IsParentOf(parentType))
                .ToArray();

            return descriptors;
        }
    }
}
﻿using System;
using System.Reflection;
using Elika.Data.Exceptions;
using Elika.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Elika.Data
{
    /// <summary>
    ///     Расширения для <see cref="IServiceCollection" />
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        ///     Добавить контекст базы данных
        /// </summary>
        /// <typeparam name="TDbContext">Тип контекста</typeparam>
        /// <param name="services">Коллекция сервисов</param>
        /// <param name="autoLoadMappings">Флаг использования автоматической загрузки мапингов сущностей</param>
        /// <param name="assemblies">Список сборок из которых получать мапинги сущностей</param>
        /// <returns>Коллекция сервисов</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="EntityAlreadyRegisteredException"></exception>
        /// <exception cref="DbContextInvalidException"></exception>
        /// <exception cref="EntityInvalidException"></exception>
        public static IServiceCollection RegisterDbContextAsRepository<TDbContext>(this IServiceCollection services, bool autoLoadMappings = true, Func<Assembly[]> assemblies = null) where TDbContext : class, IDbContext
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            services.TryAddScoped<TDbContext>();
            services.SetUpDatabaseRepositories();
            services.AddSingleton(p =>
            {
                var collection = new EntityCollection();
                collection.ApplyMappingsFromAssemblies(assemblies?.Invoke() ?? AppDomain.CurrentDomain.GetAssemblies());
                return collection;
            });
            return services;
        }

        /// <summary>
        ///     Установка настроек репозиторий баз данны в Elika
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекция сервисов</returns>
        /// <exception cref="ArgumentNullException"></exception>
        private static IServiceCollection SetUpDatabaseRepositories(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            services.TryAddScoped(typeof(IRepository<>), typeof(DbRepository<>));
            services.TryAddSingleton(services);
            services.TryAddSingleton<IEntityCollection>(p => p.GetRequiredService<EntityCollection>());
            services.TryAddScoped<EntityCollectionProvider>();
            return services;
        }
    }
}
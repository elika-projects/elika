﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Elika.Tests")]
[assembly: InternalsVisibleTo("Elika.EntityFramework")]
[assembly: InternalsVisibleTo("Elika.EntityFrameworkCore")]
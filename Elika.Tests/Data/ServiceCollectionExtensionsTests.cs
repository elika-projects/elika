﻿using Elika.Data;
using Elika.Tests.Data.TestItems;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace Elika.Tests.Data
{
    public class ServiceCollectionExtensionsTests
    {
        [Test]
        [Description("Проверка добавление одного контекста")]
        public void AddDbContextSingleTest()
        {
            MappingValidator.Register(typeof(TestDbContext), new TestMappingValidator());

            var services = new ServiceCollection()
                .RegisterDbContextAsRepository<TestDbContext>()
                .BuildServiceProvider();

            var entities = services.GetRequiredService<EntityCollection>();
            entities.AddMapping(typeof(TestMap));

            var provider = services.GetRequiredService<EntityCollectionProvider>();

            IDbContext dbContext = null;
            Assert.DoesNotThrow(() => dbContext = provider.GetDbContext<TestDomain>());
            Assert.AreEqual(typeof(TestDbContext), dbContext.GetType());
        }

        [Test]
        [Description("Проверка добавления нескольких контекстов")]
        public void AddDbContextMultipleTest()
        {
            MappingValidator.Register(typeof(TestDbContext), new TestMappingValidator());
            MappingValidator.Register(typeof(TestDbContext2), new TestMappingValidator());

            var services = new ServiceCollection()
                .RegisterDbContextAsRepository<TestDbContext>()
                .RegisterDbContextAsRepository<TestDbContext2>()
                .BuildServiceProvider();

            var entities = services.GetRequiredService<EntityCollection>();
            entities.AddMapping(typeof(TestMap));
            entities.AddMapping(typeof(Test2Map));

            var provider = services.GetRequiredService<EntityCollectionProvider>();

            IDbContext dbContext = null;
            Assert.DoesNotThrow(() => dbContext = provider.GetDbContext<TestDomain>());
            Assert.AreEqual(typeof(TestDbContext), dbContext.GetType());

            Assert.DoesNotThrow(() => dbContext = provider.GetDbContext<TestDomain2>());
            Assert.AreEqual(typeof(TestDbContext2), dbContext.GetType());
        }
    }
}